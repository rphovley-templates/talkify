import * as Knex from 'knex'

export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .createTable('user_group', (table) => {
      table.increments('id')
      table.timestamp('created_at', { useTz: true }).notNullable()
      table.timestamp('updated_at', { useTz: true }).notNullable()
      table.integer('group_id').unsigned().notNullable().references('id')
        .inTable('group')
        .index()
      table.integer('participant_id').unsigned().notNullable().references('id')
        .inTable('app_user')
        .index()
    })
}


export async function down(knex: Knex): Promise<void> {
  return knex.schema
    .dropTable('user_group')
}
