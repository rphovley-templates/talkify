import { FastifyRequest, FastifyReply } from 'fastify'

import { GroupRepository } from '../../repositories/group.respository'
import { MessageRepository } from '../../repositories/message.repository'
import { GroupCreateSchema, GroupIndexSchema } from '../../schemas/group.schema'
import { MessageCreateSchema, MessageIndexSchema } from '../../schemas/message.schema'


export async function index(request: FastifyRequest, _: FastifyReply): Promise<GroupIndexSchema.ReplyType> {
  const groupData = await GroupRepository.index(request.appUser.id)
  return { message: 'success', data: groupData }
}

export async function create(request: FastifyRequest<{Body: GroupCreateSchema.BodyType}>, reply: FastifyReply): Promise<GroupCreateSchema.ReplyType> {
  await GroupRepository.create(request.body, request.appUser.id)
  return reply.code(201).send({  message: 'success' })
}

export async function messagesIndex(request: FastifyRequest<{Params: {group_id: number}}>, _: FastifyReply): Promise<MessageIndexSchema.ReplyType> {
  const messageData = await MessageRepository.indexByGroup(request.params.group_id, request.appUser.id)
  return { message: 'success', data: messageData }
}

export async function createMessage(request: FastifyRequest<{Body: MessageCreateSchema.BodyType, Params: {group_id: number}}>, reply: FastifyReply): Promise<MessageCreateSchema.ReplyType> {
  await MessageRepository.create(request.body, request.appUser.id)
  return reply.code(201).send({  message: 'success' })
}