import { FastifyRequest, FastifyReply } from 'fastify'

import { AppUser, ISignupUser } from '../../models/app_user.model'
import { AuthErrors } from '../../utils/customer_errors'


export async function signup(request: FastifyRequest, _: FastifyReply): Promise<any> {
  // receive google data and the related attendee information
  try {
    const appUser = await AppUser.signupFirebaseUser(request.body as ISignupUser)
    return { message: 'success', data: appUser }
  } catch (err) {
    if (err.code?.startsWith('auth/')) throw new AuthErrors.FirebaseError()
    throw err
  }
}
