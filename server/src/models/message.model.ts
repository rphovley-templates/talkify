import { Model } from 'objection'
import { AppUser } from './app_user.model'
import { BaseModel, IBaseModel } from './base.model'

export interface IMessage extends IBaseModel {
  sent_by_id: number
  group_id: number
  contents: string
}

export class Message extends BaseModel implements IMessage {
  sent_by_id: number
  group_id: number
  contents: string

  static tableName = 'message'

  static relationMappings = {
    users: {
      relation: Model.HasManyRelation,
      modelClass: AppUser,
      join: {
        from: 'message.sent_by_id',
        to: 'app_user.id',
      },
    },
  }
}
