import { ValidationErrors } from '../../utils/customer_errors'

export class BaseValidation {
  static validate(body: {}): void {
    if (!body) throw new ValidationErrors.MissingBodyError()
  }
}
