import { BaseModel, IBaseModel } from './base.model'

export interface IUserGroup extends IBaseModel {
  participant_id: number
  group_id: number
}

export class UserGroup extends BaseModel implements IUserGroup {
  participant_id: number
  group_id: number

  static tableName = 'user_group'
}
