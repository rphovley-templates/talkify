import { BaseModel, IBaseModel } from './base.model'

import admin = require('firebase-admin')

// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
const roles = <const> ['admin', 'user']
export type Roles = typeof roles[number]

export interface IAppUser extends IBaseModel {
  roles: Roles[]
  auth_id: string
  auth_method: 'firebase'
  email: string
}

export interface ISignupUser extends IAppUser{
  firebase_token: string
  email: string
}

export class AppUser extends BaseModel implements IAppUser {
  roles: Roles[]
  auth_id: string
  auth_method: 'firebase'
  email: string

  static tableName = 'app_user'

  static async signupFirebaseUser(signupUser: ISignupUser): Promise<AppUser> {
    const { uid } = await admin.auth().verifyIdToken(signupUser.firebase_token) // validate firebase token
    let appUser = await AppUser.query().findOne({ auth_id: uid })

    if (!appUser) { // don't signup an existing user
      // create app user
      delete signupUser.firebase_token // remove token, don't want to insert it. Only need the firebase uid.
      signupUser.roles = ['user'] as Roles[] // ensure a attendee can't set their role
      signupUser.auth_id = uid
      signupUser.auth_method = 'firebase'
      appUser = await AppUser.query().insert(signupUser)
    }
    return appUser
  }
}
