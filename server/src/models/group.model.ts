import { Model } from 'objection'
import { BaseModel, IBaseModel } from './base.model'
import { Message } from './message.model'
import { UserGroup } from './user_group.model'

export interface IGroup extends IBaseModel {
  created_by_id: number
  name: string
  topic: string
}

export class Group extends BaseModel implements IGroup {
  created_by_id: number
  name: string
  topic: string

  static tableName = 'group'

  static relationMappings = {
    users: {
      relation: Model.HasManyRelation,
      modelClass: UserGroup,
      join: {
        from: 'group.id',
        to: 'user_group.group_id',
      },
    },
    messages: {
      relation: Model.HasManyRelation,
      modelClass: Message,
      join: {
        from: 'group.id',
        to: 'message.group_id',
      },
    }
  }
}
