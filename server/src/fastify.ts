import * as Fastify from 'fastify'
import CorsPlugin from '@fastify/cors'
import * as routes from './routes/base'
import { initGlobalObjection } from './utils/db_config'
import normalizePort from './utils/normalize_port'
import authentication from './plugins/authentication'
import { getLogger } from './utils/logger' // uses google cloud logging
import authorization from './plugins/authorization'

const PORT_FALLBACK = '8080'

export class FastifyServer {
  private fastifyServer: Fastify.FastifyInstance
  private port: string | number

  constructor() {
    this.fastifyServer = Fastify.fastify({
      logger: true,
    })
    this.port = normalizePort(process.env.SERVER_PORT || PORT_FALLBACK)
    initGlobalObjection() // initialize the db connection and objection ORM to global db
    this.initPlugins()
    this.initRoutes()
    this.appStatusRoute()
    this.initErrorHandling()
  }

  /**
   * Fire up the bass cannon
   */
  public static bootstrap(): FastifyServer {
    return new FastifyServer()
  }

  public listen(onListening): void {
    this.fastifyServer.listen(this.port, '0.0.0.0', onListening)
  }
  /**
   * Stack middleware
   */
  private initPlugins(): void {
    this.fastifyServer.register(CorsPlugin, { origin: true })
    this.fastifyServer.register(authentication)
    this.fastifyServer.register(authorization)
  }

  /**
   * Add routers to exports in routes/index.ts and they'll be added here automatically
   */
  private initRoutes(): void {
    Object.values(routes).forEach(route => {
      route.create(this.fastifyServer)
    })
  }

  private appStatusRoute(): void {
    this.fastifyServer.get('/api/status', (_, res) => {
      res.send({ message: 'okay' })
    })
  }

  private initErrorHandling(): void {
    // this.fastifyServer.use((err, req, res, next) => {
    //   if (err == null) err = {}
    //   // eslint-disable-next-line no-console
    //   console.log('\x1b[31m', err)
    //   if (err instanceof BaseError) { // Errors designed by developers to go to the end user are of type BaseError
    //     res.status(err.status).json({ message: err.message, err })
    //   } else {
    //     getLogger().error(err) // only send log to google if is not an expected error
    //     res.status(err.status || err.statusCode || 500).json({ message: err.message || 'Internal Server Error', err })
    //   }
    // })
  }
}
