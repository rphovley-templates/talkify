import { Group } from '../models/group.model'
import { UserGroup } from '../models/user_group.model'
import { GroupCreateSchema } from '../schemas/group.schema'

export namespace GroupRepository {
  export const index = async (participant_id: number): Promise<Group[]> => {
    return Group.query()
      .withGraphJoined('[users]')
      .where('users.participant_id', participant_id)
      .limit(20).orderBy('updated_at', 'DESC')
  }

  export const create = async (body: GroupCreateSchema.BodyType, created_by_id: number): Promise<void> => {
    return Group.transaction(async trx => {
      const groupData = {
        name: body.name,
        topic: body.topic,
        created_by_id: created_by_id
      } as Group
      const group = await Group.query(trx).insert(groupData)
      const userGroups = body.users.map((participant_id) => {
        return { participant_id, group_id: group.id}
      })
      await UserGroup.query(trx).insert(userGroups)
      return
    })
  }
  
}
