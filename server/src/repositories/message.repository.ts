import { Message } from "../models/message.model"
import { MessageCreateSchema } from "../schemas/message.schema"

export namespace MessageRepository {

  export const indexByGroup = async (group_id: number, participant_id: number): Promise<Message[]> => {
    return Message.query()
      .withGraphJoined('[users]')
      .where('group_id', group_id)
      .where('sent_by_id', participant_id)
      .limit(20).orderBy('updated_at', 'DESC')
  }

  export const create = async (body: MessageCreateSchema.BodyType, sent_by_id: number): Promise<void> => {
    const message = {...body, sent_by_id}
    await Message.query().insert(message)
  }
}