import { Static, Type } from '@sinclair/typebox'

export namespace GroupIndexSchema {
  const Group = Type.Object({
    id: Type.Number(),
    name: Type.String(),
    topic: Type.String(),
  })
  
  const Groups = Type.Array(Group)
  
  const Reply = Type.Object({
    message: Type.String(),
    data: Groups,
  })

  export const SuccessReply = {
    200: Reply,
  }

  export type ReplyType = Static<typeof Reply>
}

export namespace GroupCreateSchema {
  export const GroupBody = Type.Object({
    name: Type.String(),
    topic: Type.String(),
    users: Type.Array(Type.Number())
  })
  
  const Reply = Type.Object({
    message: Type.String(),
  })
  
  export const SuccessReply = {
    201: Reply,
  }

  export type BodyType = Static<typeof GroupBody>
  export type ReplyType = Static<typeof Reply>
}