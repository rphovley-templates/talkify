import { Static, Type } from '@sinclair/typebox'

export const SignupBodySchema = Type.Object({
  firebase_token: Type.String(),
  email: Type.String(),
})

export const SignupResponse = Type.Object({
  message: Type.String(),
  data: Type.Object({
    roles: Type.Array(Type.String()),
    auth_id: Type.String(),
    email: Type.String({ format: 'email' }),
    auth_method: Type.String(),
    id: Type.Integer(),
  }),
})

export type SignupSchemaType = Static<typeof SignupBodySchema>
export const SignupResponseSchema = {
  200: SignupResponse,
}
