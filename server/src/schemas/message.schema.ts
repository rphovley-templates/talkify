import { Static, Type } from '@sinclair/typebox'

export namespace MessageIndexSchema {
  const Message = Type.Object({
    id: Type.Number(),
    contents: Type.String(),
    group_id: Type.Number(),
    sent_by_id: Type.Number(),
  })
  
  const Messages = Type.Array(Message)
  
  const Reply = Type.Object({
    message: Type.String(),
    data: Messages,
  })

  export const SuccessReply = {
    200: Reply,
  }

  export type ReplyType = Static<typeof Reply>
}

export namespace MessageCreateSchema {
  export const MessageBody = Type.Object({
    contents: Type.String(),
    group_id: Type.Number(),
  })

  const Reply = Type.Object({
    message: Type.String(),
  })
  
  export const SuccessReply = {
    201: Reply,
  }

  export type BodyType = Static<typeof MessageBody>
  export type ReplyType = Static<typeof Reply>
}