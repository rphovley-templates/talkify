import admin from 'firebase-admin'

import {firebaseAccount} from './env'

const initializeFirebase = (): void => {

  if ((process.env.NODE_ENV || 'development') === 'development') { admin.database.enableLogging(true) }
  admin.initializeApp({
    credential: admin.credential.cert(firebaseAccount),
    databaseURL: process.env.DATABASE_URL,
  })
  admin.auth()
}

export { initializeFirebase, admin }
