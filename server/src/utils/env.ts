/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import * as dotenv from 'dotenv'

const env = process.env.NODE_ENV ? `${process.env.NODE_ENV}` : ''
if (env === '') {
  dotenv.config({
    path: `${__dirname}/../../bin/.db.env`,
  })
  dotenv.config({
    path: `${__dirname}/../../bin/.log.env`,
  })
}

// eslint-disable-next-line @typescript-eslint/no-var-requires
export const firebaseAccount = require(`${__dirname}/../../bin/firebase_config.json`)
