/* eslint-disable no-console */

// module dependencies

import './utils/env'

// eslint-disable-next-line import/first
import { FastifyServer } from './fastify'


// create http server

const fastifyServer = FastifyServer.bootstrap()
fastifyServer.listen((err, port) => {
  if (err) {
    console.error(err)
    process.exit(0)
  }
  console.log(`Magic Happens on ${port}`)
})
