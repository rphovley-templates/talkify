import fp from 'fastify-plugin'
import { FastifyRequest, FastifyInstance, FastifyReply, FastifyPluginAsync } from 'fastify'
import { AuthErrors, BaseError } from '../utils/customer_errors'
import { initializeFirebase, admin } from '../utils/firebase_config'
import { getLogger } from '../utils/logger'
import { AppUser } from '../models/app_user.model'


declare module 'fastify' {
  interface FastifyRequest {
    appUser: AppUser;
  }
}


export const whiteList: string[] = [
  // Add unprotected endpoints here
  '/api/status',
  '/api/auth/signup',
]
initializeFirebase()

const getBearerToken = (authHeader: string): string => {
  if (!authHeader.startsWith('Bearer') || authHeader.split(' ').length !== 2) throw new AuthErrors.UnauthorizedError('Invalid bearer token')
  return authHeader.split(' ')[1]
}

const getUser = async (token: string): Promise<AppUser> => {
  let appUser
  try {
    const fUser = await admin.auth().verifyIdToken(token) // verify firebase id token is valid
    appUser = await AppUser.query().findOne({ auth_id: fUser.uid }) // find user with firebase uid in system
    if (!appUser) throw new AuthErrors.UserNotFoundUnauthorizedError('No User found for that firebase_id. Send user to registration')
  } catch (err) {
    // TODO: fix this garbase conditional. The universe is crying out in pain
    if (err instanceof AuthErrors.UserNotFoundUnauthorizedError) { // user with that id not found
      throw err
    } else if (err.code.indexOf('id-token-expired') > -1) { // if from firebase about the token expiring
      throw new AuthErrors.TokenExpiredError()
    } else if (!err.code) { // unexpeccted error, log to google logging
      getLogger().error(err) // report error to error service if from firebase
    }
    throw new AuthErrors.UnauthorizedError()
  }
  return appUser
}

const firebaseAuthentication: FastifyPluginAsync = async (fastify: FastifyInstance, _) => {
  fastify.decorateRequest('appUser', null)
  fastify.addHook('onRequest', async (request: FastifyRequest, reply: FastifyReply) => {
    if (whiteList.find(x => x === request.url)) {
      return
    }
    try {
      if (!request.headers.authorization) throw new AuthErrors.UnauthorizedError('No Authorization token sent.')
      const token = getBearerToken(request.headers.authorization)
      const appUser = await getUser(token)
      request.appUser = appUser
    } catch (err) {
      if (err instanceof BaseError) throw err
      else throw new AuthErrors.UnauthorizedError('')
    }
  })
}


export default fp(firebaseAuthentication, '3.x')
