import fp from 'fastify-plugin'
import { FastifyRequest, FastifyInstance, FastifyReply, FastifyPluginAsync, HookHandlerDoneFunction } from 'fastify'

import { whiteList } from './authentication'
import { Roles } from '../models/app_user.model'
import { AuthErrors } from '../utils/customer_errors'


type IRoutePermissions = {
  [key: string]: Roles[]
}

// TODO: make this configurable per request method (i.e. GET, POST, PUT, etc...)
const routePermissions: IRoutePermissions = {
  '/admin/app_users': ['user'],
}

function isAdmin(user): boolean {
  return user.roles.includes('admin')
}

// Validate that the user has the correct role/permissions for the requested resource
const authorization: FastifyPluginAsync = async (fastify: FastifyInstance, _) => {
  fastify.addHook('onRequest', (request: FastifyRequest, reply: FastifyReply, done: HookHandlerDoneFunction) => {
    if (whiteList.find(x => x === request.url)) { // skip whitelisted resources
      return done()
    }
    // admins can access all resources
    if (!isAdmin(request.appUser)) {
      const routePermission = routePermissions[request.url]
      if (!routePermission) return done()

      const sufficient = routePermission.some(role => request.appUser.roles.includes(role))
      if (!sufficient) { throw new AuthErrors.PermissionDenied() }
    }
    done()
  })
}

export default fp(authorization, '3.x')
