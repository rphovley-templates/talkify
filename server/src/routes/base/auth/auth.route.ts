
import * as Fastify from 'fastify'
import { signup } from '../../../controllers/base/auth.controller'
import { SignupBodySchema, SignupResponseSchema, SignupSchemaType } from '../../../schemas/auth.schema'

export class AuthRoute {
  public static create(fastify: Fastify.FastifyInstance): void {
    fastify.post<{Body: SignupSchemaType, Reply: {}}>('/api/auth/signup', {
      schema: {
        body: SignupBodySchema,
        response: SignupResponseSchema,
      },
    }, signup)
  }
}
