
import * as Fastify from 'fastify'
import { create, createMessage, index, messagesIndex } from '../../../controllers/base/group.controller'
import { GroupCreateSchema, GroupIndexSchema } from '../../../schemas/group.schema'
import { MessageCreateSchema, MessageIndexSchema } from '../../../schemas/message.schema'

export class GroupRoute {
  public static create(fastify: Fastify.FastifyInstance): void {
    fastify.get<{Reply: GroupIndexSchema.ReplyType }>('/api/groups', {
      schema: {
        response: GroupIndexSchema.SuccessReply,
      },
    }, index)
    fastify.post<{Body: GroupCreateSchema.BodyType, Reply: GroupCreateSchema.ReplyType }>('/api/groups', {
      schema: {
        body: GroupCreateSchema.GroupBody,
        response: GroupCreateSchema.SuccessReply,
      },
    }, create )
    fastify.get<{Reply: MessageIndexSchema.ReplyType, Params: {group_id: number} }>('/api/groups/:group_id/messages', {
      schema: {
        response: MessageIndexSchema.SuccessReply,
      },
    }, messagesIndex)
    fastify.post<{Body: MessageCreateSchema.BodyType, Reply: MessageCreateSchema.ReplyType, Params: {group_id: number} }>('/api/groups/:group_id/messages', {
      schema: {
        body: MessageCreateSchema.MessageBody,
        response: MessageCreateSchema.SuccessReply,
      },
    }, createMessage)
  }
}
